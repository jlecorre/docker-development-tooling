#
## @brief Dockerfile used to build a Debian image including
## a pseudo development environment (sysadmin like)
## @author Joel LE CORRE <git@sublimigeek.fr>
#

# Based on official Debian image
FROM debian:buster

# Add labels to describe image usage
LABEL debian_release 10 (buster)
LABEL architecture amd64
LABEL context development-tooling
LABEL maintainer "Joel LE CORRE <git@sublimigeek.fr>"

# Timezone configuration
ENV TZ Europe/Paris

# Force non interactive installation of packages
ENV DEBIAN_FRONTEND noninteractive

# ~~~~~~~~~~~~~~~~~~~~~
# Environment variables
# ~~~~~~~~~~~~~~~~~~~~~

ENV USERNAME "jlecorre"
# https://golang.org/dl/
ENV GOLANG_VERSION "1.12.7"
# https://github.com/ansible/ansible/releases
ENV ANSIBLE_VERSION "2.7.12"
# https://github.com/ansible/ansible-lint/releases
ENV ANSIBLE_LINT_VERSION "4.1.0"
# https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html
ENV OPENSTACK_CLIENT_VERSION "3.19.0"
# https://kubernetes.io/docs/tasks/tools/install-kubectl/
ENV KUBECTL_VERSION "1.15.1-00"
# https://learn.hashicorp.com/terraform/getting-started/install.html
ENV TERRAFORM_VERSION "0.11.14"
ENV TERRAFORM_ARCH "linux_amd64"

# Updating system
RUN \
  apt-get update && \
  apt-get -y dist-upgrade

# Installation of useful tools
RUN \
  apt-get install -y apt-utils git screen python3 pass procps \
  vim wget curl telnet jq shellcheck ca-certificates gettext \
  gnupg make openssl openssh-client python3-pytest python-pip sudo \
  yamllint bash-completion htop net-tools dnsutils apt-transport-https unzip jid

# Ansible installation
RUN \
  wget --quiet https://github.com/ansible/ansible/archive/v${ANSIBLE_VERSION}.tar.gz -O - | tar xz -C /opt && \
  mv /opt/ansible-${ANSIBLE_VERSION} /opt/ansible && \
  cd /opt/ansible && \
  pip install -r ./requirements.txt && \
  python setup.py install

# Ansible linter installation
RUN \
  pip install ansible-lint==${ANSIBLE_LINT_VERSION}

# Link host's user in container
RUN \
  useradd -d /home/${USERNAME} ${USERNAME}

# Add user in sudoers group
RUN \
  echo "${USERNAME} ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/${USERNAME}

# OpenStack CLI installation
RUN \
  pip install python-openstackclient==${OPENSTACK_CLIENT_VERSION}

# Kubectl CLI installation
RUN \
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
  echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
  apt-get update && \
  apt-get install -y kubectl=${KUBECTL_VERSION}

# Terraform installation
RUN \
  mkdir -p /opt/terraform && \
  wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TERRAFORM_ARCH}.zip -O /opt/terraform/terraform.zip && \
  unzip -q /opt/terraform/terraform.zip -d /opt/terraform/  && \
  chmod +x /opt/terraform/terraform

# Golang installation
RUN \
  wget --quiet https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz -O - | tar xz -C /opt/ && \
  apt install go-dep

# Environment variables used by Golang
# https://golang.org/dl/
ENV GOPATH "/home/$USERNAME/go"
ENV GOEXEC "/opt/go/bin/go"

# Venom installation
# https://github.com/ovh/venom
RUN \
  ${GOEXEC} get -u github.com/ovh/venom && \
  cd ${GOPATH}/src/github.com/ovh/venom; dep ensure && \
  ${GOEXEC} install github.com/ovh/venom/cli/venom

# CFSSL installation
RUN \
  ${GOEXEC} get -u github.com/cloudflare/cfssl/cmd/cfssl && \
  ${GOEXEC} get -u github.com/cloudflare/cfssl/cmd/cfssljson

# Image cleanup
RUN \
  apt-get -y autoremove && \
  apt-get -y autoclean && \
  apt-get -y clean && \
  rm -rf /var/lib/apt/lists/*

# Switch container with the good user workspace
USER ${USERNAME}

# Force workspace to use
WORKDIR /home/${USERNAME}

# Keep containuer up and running
CMD ["sleep", "infinity"]
